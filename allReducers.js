'Use Strict';

import {combineReducers} from 'redux';

const allReducers = () => {
    const GalleryReducer = require("./js/gallery/reducer/GalleryReducer").default;

    return combineReducers({
        GalleryReducer
    })
};

export default allReducers;
