package com.assignment;

import android.content.pm.PackageManager;

import com.facebook.react.ReactActivity;

public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "Assignment";
  }

  @Override
  public int checkPermission(String permission, int pid, int uid) {
    return PackageManager.PERMISSION_GRANTED;
  }

  @Override
  public int checkSelfPermission(String permission) {
    return PackageManager.PERMISSION_GRANTED;
  }

  @Override
  public boolean shouldShowRequestPermissionRationale(String permission) {
    return false;
  }
}
