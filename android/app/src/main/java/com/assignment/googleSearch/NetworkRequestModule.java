package com.assignment.googleSearch;

import android.util.Log;

import com.assignment.googleSearch.data.Api;
import com.assignment.googleSearch.data.NetworkRequestApi;
import com.assignment.googleSearch.data.ResponseData;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableNativeMap;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

class NetworkRequestModule extends ReactContextBaseJavaModule {
    private static final String TAG = "NetworkRequestModule";
    private Api api;
    private Retrofit retrofit;
    private NetworkRequestApi networkRequestApi;
    private ReactApplicationContext reactContext;

    public NetworkRequestModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
        api = new Api(reactContext);
        retrofit = api.getRetrofit();
        networkRequestApi = retrofit.create(NetworkRequestApi.class);
    }

    @Override
    public String getName() {
        return TAG;
    }

    @ReactMethod
    public void getData(final Callback onSuccess, final Callback onError) {
        Call<ArrayList<ResponseData>> data = networkRequestApi.getData();
        data.enqueue(new retrofit2.Callback<ArrayList<ResponseData>>() {
            @Override
            public void onResponse(Call<ArrayList<ResponseData>> call, Response<ArrayList<ResponseData>> response) {
//                Log.d("mydebug success", response.body().toString());
                try {
                    Gson g = new Gson();
                    WritableArray array = new WritableNativeArray();
                    for (ResponseData co : response.body()) {
                        JSONObject jo = null;

                        jo = new JSONObject(g.toJson(co));

                        WritableMap wm = convertJsonToMap(jo);
                        array.pushMap(wm);
                    }
                    onSuccess.invoke(array);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ResponseData>> call, Throwable t) {
//                Log.d("mydebug error", "something went wrong");
                onError.invoke("something went wrong");
            }
        });
    }

    private static WritableMap convertJsonToMap(JSONObject jsonObject) throws JSONException {
        WritableMap map = new WritableNativeMap();

        Iterator<String> iterator = jsonObject.keys();
        while (iterator.hasNext()) {
            String key = iterator.next();
            Object value = jsonObject.get(key);
            if (value instanceof JSONObject) {
                map.putMap(key, convertJsonToMap((JSONObject) value));
            } else if (value instanceof Boolean) {
                map.putBoolean(key, (Boolean) value);
            } else if (value instanceof Integer) {
                map.putInt(key, (Integer) value);
            } else if (value instanceof Double) {
                map.putDouble(key, (Double) value);
            } else if (value instanceof String) {
                map.putString(key, (String) value);
            } else {
                map.putString(key, value.toString());
            }
        }
        return map;
    }
}
