package com.assignment.googleSearch.data;

import android.content.Context;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {

    final int CACHE_SIZE = 10 * 1024 * 1024; // 10 MB
    private Context context;

    public Api(Context context) {
        this.context = context;
    }

    public Retrofit getRetrofit() {
        Cache cache = new Cache(context.getCacheDir(), CACHE_SIZE);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .cache(cache)
                .build();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://5dbfc096e295da001400b90d.mockapi.io/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();
        return retrofit;
    }
}
