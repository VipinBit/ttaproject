package com.assignment.googleSearch.data;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface NetworkRequestApi {

    @GET("codingChallenge/v1/images")
    Call<ArrayList<ResponseData>> getData();
}
