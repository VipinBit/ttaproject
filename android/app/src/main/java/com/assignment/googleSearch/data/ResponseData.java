package com.assignment.googleSearch.data;

public class ResponseData {

    String avatar;
    String imageUrl;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "ResponseData{" +
                "avatar='" + avatar + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
