/**
 * @format
 */

'Use Strict';

import {AppRegistry} from 'react-native';
import React, {Component} from 'react';
import {applyMiddleware, compose, createStore} from 'redux';
import {Provider} from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import allReducers from './allReducers';
import rootSaga from './rootSaga';
import GalleryContainer from './js/gallery/container/GalleryContainer';

const sagaMiddleware = createSagaMiddleware();
let middlewares = compose(applyMiddleware(sagaMiddleware));
let store = createStore(allReducers(), middlewares);

class Index extends Component {

    render() {
        return <Provider store={store}>
            <GalleryContainer/>
        </Provider>;
    }

}

sagaMiddleware.run(rootSaga);

AppRegistry.registerComponent('Assignment', () => Index);

export default store;

