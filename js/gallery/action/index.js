'use strict';


import {TYPE_GET_DATA, TYPE_SEND_DATA} from './actionType';

export const getData = () => {
    return {
        type: TYPE_GET_DATA,
    };
};

export const sendData = (data) => {
    return {
        type: TYPE_SEND_DATA,
        data,
    };
};
