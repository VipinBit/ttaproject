import React from 'react';
import {Dimensions, FlatList, Image, Text, View} from 'react-native';

const screenWidth = Dimensions.get('window').width;

export class GalleryComponent extends React.Component {

    constructor(props) {
        super(props);
        this.listRef = React.createRef();
        this.state = {
            numberOfColumns: 3,
            data: [],
        };
    }

    componentDidMount() {
        this.getData();
    };

    getData = () => {
        this.props.getData();
    };

    _renderItem = ({item, index}) => {

        let heightWidth = screenWidth / this.state.numberOfColumns;
        return (
            <View style={{height: heightWidth, width: heightWidth, padding: 2}} key={index.toString()}>
                <Image resizeMode={'cover'} source={{uri: item.imageUrl}} style={{flex: 1}}/>
            </View>
        );
    };

    changeColumn = () => {
        this.setState({
            numberOfColumns: 5,
        });
    };


    render() {
        return (
            <View style={{flex: 1}}>
                <Text onPress={this.changeColumn} style={{backgroundColor: 'grey', width: '100%', paddingVertical: 8}}>Change
                    column</Text>
                <FlatList
                    key={this.state.numberOfColumns}
                    ref={this.listRef}
                    data={this.props.data}
                    extraData={this.props.data}
                    onEndReachedThreshold={0.5}
                    onEndReached={this.getData}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={this._renderItem}
                    numColumns={this.state.numberOfColumns}
                />
            </View>
        );
    }

}
