'use strict';

import {connect} from 'react-redux';
import {getData} from '../action';
import {GalleryComponent} from '../component/GalleryComponent';

const mapStateToProps = (state) => {
    return {
        data: state.GalleryReducer.data,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getData: () => {
            dispatch(getData());
        },
    };
};

const GalleryContainer = connect(mapStateToProps, mapDispatchToProps)(GalleryComponent);
export default GalleryContainer;
