import {TYPE_SEND_DATA} from '../action/actionType';

const INITIAL_STATE = {
    data: [],
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TYPE_SEND_DATA: {
            let newData = [...state.data, ...action.data];
            return {
                ...state,
                data: newData,
            };
        }
        default:
            return {
                ...state,
            };
    }
}
