import {NativeModules} from 'react-native';

let networkRequestModule = NativeModules.NetworkRequestModule;

function getData() {
    return new Promise((resolve, reject) => {
        networkRequestModule.getData((res) => {
            return resolve(res);
        }, (error) => {
            return reject(error);
        });
    });
}

export const Api = {
    getData,
};
