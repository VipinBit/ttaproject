'use strict';
import {put, takeLatest} from 'redux-saga/effects';
import {sendData} from '../action';
import {TYPE_GET_DATA} from '../action/actionType';
import {Api} from './Api';

function* getData() {
    const data = yield Api.getData();
    yield put(sendData(data));
}

export default function* GalleryWatcher() {
    yield takeLatest(TYPE_GET_DATA, getData);
}
