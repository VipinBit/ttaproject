'Use Strict';
import {all, call} from 'redux-saga/effects';


export default function* rootSaga() {
    const GalleryWatcher = require('./js/gallery/saga/GallerySaga').default;

    return yield all([
        call(GalleryWatcher),
    ]);
}
